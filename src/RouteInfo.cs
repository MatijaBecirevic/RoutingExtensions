﻿using System.Collections.Generic;

namespace RoutingExtensions.Internals
{
	internal class RouteInfo
	{
		public string ActionName { get; set; }
		public string ControllerName { get; set; }
		public IDictionary<string, object> RouteValues { get; set; }
	}
}
