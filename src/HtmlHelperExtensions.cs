﻿using System;
using System.Linq.Expressions;
using System.Web.Mvc;
using System.Web.Mvc.Html;

namespace RoutingExtensions
{
	public static class HtmlHelperExtensions
	{
		public static MvcHtmlString ActionFor<T>(this HtmlHelper html, Expression<Func<T, ActionResult>> expression)
			where T : Controller
		{
			var routeInfo = expression.ExtractRouteInfo();

			return html.Action(
				actionName: routeInfo.ActionName,
				controllerName: routeInfo.ControllerName,
				routeValues: routeInfo.RouteValues.ToRouteValueDictionary());
		}

		public static MvcHtmlString ActionLinkFor<T>(this HtmlHelper html, Expression<Func<T, ActionResult>> expression, string linkText, object htmlAttributes)
			where T : Controller
		{
			var routeInfo = expression.ExtractRouteInfo();

			return html.ActionLink(
				linkText: linkText,
				actionName: routeInfo.ActionName,
				controllerName: routeInfo.ControllerName,
				routeValues: routeInfo.RouteValues.ToRouteValueDictionary(),
				htmlAttributes: htmlAttributes);
		}

		public static MvcForm BeginFormFor<T>(
			this HtmlHelper html,
			Expression<Func<T, ActionResult>> expression,
			FormMethod method = FormMethod.Post,
			object htmlAttributes = null)
			where T : Controller
		{
			var routeInfo = expression.ExtractRouteInfo();

			return html.BeginForm(
				actionName: routeInfo.ActionName,
				controllerName: routeInfo.ControllerName,
				routeValues: routeInfo.RouteValues.ToRouteValueDictionary(),
				method: method,
				htmlAttributes: htmlAttributes);
		}
	}
}
