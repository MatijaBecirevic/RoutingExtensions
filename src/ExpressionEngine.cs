﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Web.Mvc;
using System.Web.Routing;
using RoutingExtensions.Internals;

namespace RoutingExtensions
{
	internal static class ExpressionEngine
	{
		private const string _methodCallOnlyExceptionMessage = "Expression can only be a method call on the type \"{0}\"!";
		private const string _methodArgumentsExceptionMessage = "You can only use constants or members as method parameters!";
		private const string _controllerNameSuffix = "Controller";
		private const BindingFlags _flags = BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.Static;

		internal static RouteInfo ExtractRouteInfo<T>(this Expression<Func<T, ActionResult>> expression)
			where T : Controller
		{
			var body = expression.Body as MethodCallExpression;

			if ( body == null )
			{
				throw GetExceptionForWrongExpressionType<T>();
			}

			return new RouteInfo
			{
				ActionName = body.Method.Name,
				ControllerName = body.GetControllerNameWithoutSuffix(),
				RouteValues = body.ExtractMethodInvocationArguments()
			};
		}

		private static ApplicationException GetExceptionForWrongExpressionType<T>()
			=> new ApplicationException(string.Format(_methodCallOnlyExceptionMessage, typeof(T).Name));

		private static string GetControllerNameWithoutSuffix(this MethodCallExpression e)
		{
			var nameWithSuffix = e.Object.Type.Name;

			if ( !nameWithSuffix.Contains(_controllerNameSuffix) )
			{
				return nameWithSuffix;
			}

			var indexOfSuffixBeggining = nameWithSuffix.IndexOf(_controllerNameSuffix);

			return nameWithSuffix.Substring(0, indexOfSuffixBeggining);
		}

		private static IDictionary<string, object> ExtractMethodInvocationArguments(this MethodCallExpression expression)
		{
			var parameterValues = expression.Arguments
				.Select(a => a.ResolveExpressionValue())
				.ToList();

			var parameterNames = expression.Method
				.GetParameters()
				.Select(p => p.Name);

			return parameterNames
				.Zip(parameterValues,
					(key, value) => new { Key = key, Value = value })
				.ToDictionary(
					pair => pair.Key,
					pair => pair.Value);
		}

		private static object ResolveExpressionValue(this Expression expression)
		{
			if ( expression is ConstantExpression )
			{
				return (expression as ConstantExpression).Value;
			}

			if ( expression is MethodCallExpression )
			{
				var methodExpression = (expression as MethodCallExpression);
				var arguments = methodExpression
					.ExtractMethodInvocationArguments()
					.Select(pair => pair.Value)
					.ToArray();

				var obj = methodExpression.Object?.ResolveExpressionValue();

				return methodExpression.Method.Invoke(obj, arguments);
			}

			return expression
				.ResolveMemberExpression()
				.GetValue();
		}

		private static MemberExpression ResolveMemberExpression(this Expression expression)
		{
			if ( expression is MemberExpression || expression is ConstantExpression )
			{
				return expression as MemberExpression;
			}
			else if ( expression is UnaryExpression )
			{
				return (expression as UnaryExpression).Operand as MemberExpression;
			}
			else
			{
				throw new ApplicationException(_methodArgumentsExceptionMessage);
			}
		}

		private static object GetValue(this MemberExpression expression)
		{
			if ( expression.Expression is ConstantExpression )
			{
				var type = (expression.Expression as ConstantExpression)
					.Value
					.GetType();

				if ( expression.Member.IsField() )
				{
					var x = type
						.GetField(expression.Member.Name, _flags)
						.GetValue(((ConstantExpression) expression.Expression).Value);

					return x;
				}

				if ( expression.Member.IsProperty() )
				{
					return type
						.GetProperty(expression.Member.Name, _flags)
						.GetValue(((ConstantExpression) expression.Expression).Value);
				}

				return null;
			}
			else if ( expression.Expression is MemberExpression )
			{
				return (expression.Expression as MemberExpression).GetValue();
			}
			else if ( expression.Expression == null && expression.NodeType == ExpressionType.MemberAccess )
			{
				return expression
						.Member
						.GetValue(_flags);
			}
			else
			{
				throw new ApplicationException(_methodArgumentsExceptionMessage);
			}
		}

		private static object GetValue(this MemberInfo member, BindingFlags flags)
		{
			if ( member.IsField() )
			{
				return member.ReflectedType
					.GetField(member.Name, flags)
					.GetValue(null);
			}

			if ( member.IsProperty() )
			{
				return member.ReflectedType
					.GetProperty(member.Name, flags)
					.GetValue(null);
			}

			return null;
		}

		private static bool IsField(this MemberInfo member)
			=> member.MemberType == MemberTypes.Field;

		private static bool IsProperty(this MemberInfo member)
			=> member.MemberType == MemberTypes.Property;

		public static RouteValueDictionary ToRouteValueDictionary(this IDictionary<string, object> dict)
			=> new RouteValueDictionary(dict);
	}
}
