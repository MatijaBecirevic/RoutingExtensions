﻿using System;
using System.Linq.Expressions;
using System.Web.Mvc;

namespace RoutingExtensions
{
	public static class UrlHelperExtensions
	{
		public static string ActionFor<T>(this UrlHelper url, Expression<Func<T, ActionResult>> expression)
			where T : Controller
		{
			var routeInfo = expression.ExtractRouteInfo();

			return url.Action(
				actionName: routeInfo.ActionName,
				controllerName: routeInfo.ControllerName,
				routeValues: routeInfo.RouteValues.ToRouteValueDictionary());
		}
	}
}
